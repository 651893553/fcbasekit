#
# Be sure to run `pod lib lint FCBaseKit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = "FCBaseKit"
s.version          = "0.1.0"
s.summary          = "FCBaseKit 是iOS开发的基础框架"
s.homepage         = "https://bitbucket.org/651893553/fcbasekit"
s.license          = 'MIT'
s.author           = { "XFCBaseKit" => "651893553@qq.com" }
s.source           = { :git => "https://bitbucket.org/651893553/fcbasekit", :tag => s.version.to_s}

s.platform     = :ios, '7.0'
s.requires_arc = true


s.framework             = "CoreFoundation","Foundation","CoreGraphics","Security","UIKit"
s.libraries		= "z.1.1.3","stdc++","sqlite3"



s.dependency 'MJRefresh'
s.dependency 'DZNEmptyDataSet'
s.dependency 'MISAlertController'
s.dependency 'IQKeyboardManager'
s.dependency 'XXNibBridge', '~> 2.1'

s.subspec 'Base' do |base|
base.source_files = 'Pod/Classes/Base/**/*'
base.public_header_files = 'Pod/Classes/Base/**/*.h'
base.dependency 'AFNetworking'
base.dependency 'ReactiveCocoa','2.4.4'
base.dependency 'TMCache'
base.dependency 'JSONModel'
base.dependency 'FCBaseKit/Tools'
base.dependency 'FCBaseKit/Category'
end

s.subspec 'Category' do |category|
category.source_files = 'Pod/Classes/Category/**/*'
category.public_header_files = 'Pod/Classes/Category/**/*.h'
category.dependency 'MBProgressHUD'
category.dependency 'SDWebImage'
end

s.subspec 'CustomUI' do |customUI|
customUI.source_files = 'Pod/Classes/CustomUI/**/*'
customUI.public_header_files = 'Pod/CustomUI/Category/**/*.h'
customUI.dependency 'FCBaseKit/Category'
end

s.subspec 'Tools' do |tools|
tools.source_files = 'Pod/Classes/Tools/**/*'
tools.public_header_files = 'Pod/Classes/Tools/**/*.h'
end



end
