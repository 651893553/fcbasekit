# FCBaseKit

[![CI Status](http://img.shields.io/travis/XFCBaseKit/FCBaseKit.svg?style=flat)](https://travis-ci.org/XFCBaseKit/FCBaseKit)
[![Version](https://img.shields.io/cocoapods/v/FCBaseKit.svg?style=flat)](http://cocoapods.org/pods/FCBaseKit)
[![License](https://img.shields.io/cocoapods/l/FCBaseKit.svg?style=flat)](http://cocoapods.org/pods/FCBaseKit)
[![Platform](https://img.shields.io/cocoapods/p/FCBaseKit.svg?style=flat)](http://cocoapods.org/pods/FCBaseKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FCBaseKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "FCBaseKit"
```

## Author

XFCBaseKit, 651893553@qq.com

## License

FCBaseKit is available under the MIT license. See the LICENSE file for more info.
