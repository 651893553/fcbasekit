//
//  CustomTabBarController.m
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "CustomTabBarController.h"

@interface CustomTabBarController ()

@end

@implementation CustomTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configTabbar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

/**
 *  配置tabbar跳转逻辑
 */
- (void)configTabbar
{
    NSMutableArray *vcs=[NSMutableArray arrayWithCapacity:3];//创建一个数组来保存controller对象
    
    UIStoryboard *destination=[UIStoryboard storyboardWithName:@"Destination" bundle:[NSBundle mainBundle]];//首先找到对应的storyboard
    UIStoryboard *chat=[UIStoryboard storyboardWithName:@"Chat" bundle:[NSBundle mainBundle]];
    UIStoryboard *journey=[UIStoryboard storyboardWithName:@"Journey" bundle:[NSBundle mainBundle]];
    UIStoryboard *service=[UIStoryboard storyboardWithName:@"Service" bundle:[NSBundle mainBundle]];
    
    
    [vcs addObject:destination.instantiateInitialViewController];
    [vcs addObject:chat.instantiateInitialViewController];
    [vcs addObject:journey.instantiateInitialViewController];
    [vcs addObject:service.instantiateInitialViewController];
    
    
    [self setViewControllers:vcs animated:NO];//用当前的viewController数组替换原本的tabbarControlle的 viewControllers数组

    [self setTaBbarStyle];
}

/**
 *  设置tabbarImage
 */
- (void)setTaBbarStyle
{
    NSArray *images = @[@"destination", @"talk", @"stroke", @"service"];
    NSArray *imageSelects = @[@"destination_click", @"talk_click", @"stroke_click", @"service_click"];
    NSArray *titles = @[@"目的地", @"聊天", @"行程", @"客服"];
    
    for (int i = 0; i < self.viewControllers.count; i++) {
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:titles[i] image:[UIImage imageNamed:images[i]] selectedImage:[UIImage imageNamed:imageSelects[i]]];
        ((UIViewController *)self.viewControllers[i]).tabBarItem = item;
    }

}

@end
