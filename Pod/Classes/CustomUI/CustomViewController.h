//
//  CustomViewController.h
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, NavigationBarPosition) {
    NAV_LEFT,
    NAV_RIGHT,
};

@interface CustomViewController : UIViewController

- (void)showBarButton:(NavigationBarPosition)position title:(NSString *)name fontColor:(UIColor *)color;
- (void)showBarButton:(NavigationBarPosition)position imageName:(NSString *)imageName;
- (void)showBarButton:(NavigationBarPosition)position button:(UIButton *)button;
- (void)leftButtonTouch;
- (void)rightButtonTouch;
- (void)setTitleView:(UIView *)titleView;

@end
