//
//  CustomNavigationBar.m
//  TalkWord
//
//  Created by fucheng on 15/6/16.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "CustomNavigationBar.h"

@implementation CustomNavigationBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize osize = [super sizeThatFits:size];
    osize.height=10;
    return osize;
}


@end
