//
//  CustomLabel.m
//  TalkWord
//
//  Created by xufucheng on 15/5/29.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel

-(id)initWithFrame:(CGRect)frame andInsets:(UIEdgeInsets)insets
{
    self =[super initWithFrame:frame];
    if(self){
        self.insets = insets;
    }
    return self;
}

-(id)initWithInsets:(UIEdgeInsets)insets
{
    self =[super init];
    if(self){
        self.insets = insets;
    }
    return self;
}

-(void)drawTextInRect:(CGRect)rect {
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect,self.insets)];
}

@end
