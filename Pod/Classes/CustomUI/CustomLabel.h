//
//  CustomLabel.h
//  TalkWord
//
//  Created by xufucheng on 15/5/29.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel
@property(assign,nonatomic) UIEdgeInsets insets;


-(id)initWithFrame:(CGRect)frame andInsets: (UIEdgeInsets)insets;
-(id)initWithInsets: (UIEdgeInsets)insets;

@end
