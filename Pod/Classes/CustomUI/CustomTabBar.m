
//
//  CustomTabBar.m
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "CustomTabBar.h"

@implementation CustomTabBar

/**
 *  重写返回tabbar的大小
 *
 *  @param size tabbar本来应该的大小
 *
 *  @return 重新设置好的size
 */
- (CGSize)sizeThatFits:(CGSize)size
{
    CGSize osize = [super sizeThatFits:size];
//    osize.height=40;
    return osize;
}


@end
