//
//  UIButton+CYNavigationbutton.h
//  TalkWord
//
//  Created by xufucheng on 15/5/27.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (CYNavigationbutton)
-(UIButton *)initNavigationButton:(UIImage *)image;
-(UIButton *)initNavigationButtonWithTitle:(NSString *)str color:(UIColor *)color;

@end
