//
//  NSObject+CYExpand.h
//  TalkWord
//
//  Created by xufucheng on 15/5/27.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (CYExpand)

-(BOOL)isNotEmpty;

@end
