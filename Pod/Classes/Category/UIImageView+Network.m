//
//  UIImageView+Network.m
//  rssreader
//
//  Created by zhuchao on 15/2/18.
//  Copyright (c) 2015年 zhuchao. All rights reserved.
//

#import "UIImageView+Network.h"
#import <UIImageView+WebCache.h>

@implementation UIImageView (Network)

-(void)net_sd_setImageWithURL:(NSURL *)url{
    [self net_sd_setImageWithURL:url placeholderImage:nil];
}

/* 失败后重试
     SDWebImageRetryFailed = 1 << 0,
      
     //UI交互期间开始下载，导致延迟下载比如UIScrollView减速。
     SDWebImageLowPriority = 1 << 1,
      
     //只进行内存缓存
     SDWebImageCacheMemoryOnly = 1 << 2,
      
     //这个标志可以渐进式下载,显示的图像是逐步在下载
     SDWebImageProgressiveDownload = 1 << 3,
      
     //刷新缓存
     SDWebImageRefreshCached = 1 << 4,
      
     //后台下载
     SDWebImageContinueInBackground = 1 << 5,
      
     //NSMutableURLRequest.HTTPShouldHandleCookies = YES;
      
     SDWebImageHandleCookies = 1 << 6,
      
     //允许使用无效的SSL证书
     //SDWebImageAllowInvalidSSLCertificates = 1 << 7,
      
     //优先下载
     SDWebImageHighPriority = 1 << 8,
      
     //延迟占位符
     SDWebImageDelayPlaceholder = 1 << 9,
      
     //改变动画形象
     SDWebImageTransformAnimatedImage = 1 << 10, */
- (void)net_sd_setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder{
    
    [self sd_setImageWithURL:url placeholderImage:placeholder];
}

@end
