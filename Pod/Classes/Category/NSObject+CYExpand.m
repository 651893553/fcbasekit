//
//  NSObject+CYExpand.m
//  TalkWord
//
//  Created by xufucheng on 15/5/27.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "NSObject+CYExpand.h"

@implementation NSObject (CYExpand)

-(BOOL)isNotEmpty{
    return !(self == nil
             || [self isKindOfClass:[NSNull class]]
             || ([self respondsToSelector:@selector(length)]
                 && [(NSData *)self length] == 0)
             || ([self respondsToSelector:@selector(count)]
                 && [(NSArray *)self count] == 0));
    
}

@end
