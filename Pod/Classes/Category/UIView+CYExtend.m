//
//  UIView+RoundCorners.m
//  TalkWord
//
//  Created by xufucheng on 15/5/29.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "UIView+CYExtend.h"
#import <objc/runtime.h>


@implementation UIView (CYExtend)

- (void)addRoundCorners:(CGFloat)cornerRadius barderWidth:(CGFloat)width borderColor:(UIColor *)color
{
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = cornerRadius > 0;
    self.layer.borderWidth = width;
    if (color) {
        self.layer.borderColor = color.CGColor;
    }
}

- (UIViewController*)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;  
}



#pragma mark- view边框颜色
- (UIColor *)borderColor {
    
    return objc_getAssociatedObject(self, @selector(borderColor));
    
}

- (void)setBorderColor:(UIColor *)borderColor
{
    objc_setAssociatedObject(self, @selector(borderColor), borderColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

    [self setBorderColorFormUI:borderColor];
}

- (void)setBorderColorFormUI:(UIColor *)color
{
    self.layer.borderColor = color.CGColor;
    
}

#pragma mark- view的边框宽度
- (NSNumber *)borderWidth
{
    return objc_getAssociatedObject(self, @selector(borderWidth));

}

- (void)setBorderWidth:(NSNumber *)borderWidth
{
    objc_setAssociatedObject(self, @selector(borderWidth), borderWidth, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    [self setBorderWidthFromUI:borderWidth];
}

-(void)setBorderWidthFromUI:(NSNumber *)width
{
    self.layer.borderWidth = [width floatValue];
}

@end
