//
//  UIView+RoundCorners.h
//  TalkWord
//
//  Created by xufucheng on 15/5/29.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CYExtend)

@property(nonatomic, strong) IBInspectable UIColor *borderColor;
@property(nonatomic, strong) IBInspectable NSNumber *borderWidth;


- (void)setBorderColor:(UIColor *)borderColor;
- (void)setBorderWidth:(NSNumber *)borderWidth;


- (void)addRoundCorners:(CGFloat)cornerRadius barderWidth:(CGFloat)width borderColor:(UIColor *)color;
- (UIViewController*)viewController;
@end
