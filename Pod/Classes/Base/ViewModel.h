//
//  Request.h
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "Action.h"
#import "RACEXTScope.h"
#import "ReactiveCocoa.h"
#import "Request.h"

@interface ViewModel : NSObject

@property(nonatomic,strong)Action *action;
+ (id)viewModel;
- (void)handleActionMsg:(Request *)msg;
- (void)DO_DOWNLOAD:(Request *)req;
- (void)SEND_ACTION:(Request *)req;
- (void)SEND_CACHE_ACTION:(Request *)req;
- (void)SEND_NO_CACHE_ACTION:(Request *)req;
- (void)SEND_IQ_ACTION:(Request *)req;
- (void)loadViewModel;
@end
