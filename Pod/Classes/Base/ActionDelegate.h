//
//  ActionDelegate.h
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ActionDelegate <NSObject>
-(void)handleActionMsg:(Request *)msg;
@optional
-(void)handleProgressMsg:(Request *)msg;
@end
