//
//  Request.h
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//
#import "AFNetworking.h"
#import "Request.h"
#import "ActionDelegate.h"
#import "GlobalDefine.h"

@interface Action : NSObject
INTERFACE_SINGLETON(Action)
@property(nonatomic,weak)id<ActionDelegate> aDelegaete;
+(void)actionConfigHost:(NSString *)host client:(NSString *)client codeKey:(NSString *)codeKey rightCode:(NSInteger)rightCode msgKey:(NSString *)msgKey;
+ (id)Action;
- (id)initWithCache;
- (void)success:(Request *)msg;
- (void)error:(Request *)msg;
- (void)failed:(Request *)msg;
- (void)useCache;
- (void)readFromCache;
- (void)notReadFromCache;
- (AFHTTPRequestOperation *)Send:(Request *) msg;
- (AFHTTPRequestOperation *)Download:(Request *)msg;
@end
