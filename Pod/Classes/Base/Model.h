//
//  Model.h
//  TalkWord
//
//  Created by xufucheng on 15/5/27.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//

#import "JSONModel.h"

@interface Model : JSONModel

+(id)Model;
-(void)loadModel;

@end
