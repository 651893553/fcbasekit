//
//  TalkWord
//
//  Created by xufucheng on 15/5/22.
//  Copyright (c) 2015年 xufucheng. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SysTool : NSObject
+ (SysTool *)sharedInstance;
///////////////////////
//发送邮件
+ (void)sendMail:(NSString *)mail;
//打电话
+ (void)makePhoneCall:(NSString *)tel;
//发短信
+ (void)sendSMS:(NSString *)tel;
//打开URL
+ (void)openURLWithSafari:(NSString *)url;
//计算字符个数
+ (int)countWords:(NSString *)s;
//屏幕截图，并保存到相册
+ (void)saveImageFromToPhotosAlbum:(UIView *)view;
- (void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo;

//获取文件夹大小
+ (NSString *)getFolderSize:(long long)size;
+ (long long)folderSizeAtPath:(NSString *)folderPath;
+ (void)deleteAll:(NSString *)cachePath;

@end
