//
//  FCAppDelegate.h
//  FCBaseKit
//
//  Created by CocoaPods on 06/23/2015.
//  Copyright (c) 2014 XFCBaseKit. All rights reserved.
//

@import UIKit;

@interface FCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
