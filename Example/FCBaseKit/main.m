//
//  main.m
//  FCBaseKit
//
//  Created by XFCBaseKit on 06/23/2015.
//  Copyright (c) 2014 XFCBaseKit. All rights reserved.
//

@import UIKit;
#import "FCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FCAppDelegate class]));
    }
}
